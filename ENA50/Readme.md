# ENA50
The ENA50 includes the following files:
- **dHCP_lut.txt**: lut table from the dHCP parcellation scheme (associated to the ENA50_DEM_labels.nii.gz file)
- **ENA33_lut.txt**: lut table from the ENA33 parcellation scheme (associated to the ENA50_ENA_labels.nii.gz file)
- **ENA50_AD.nii.gz**: Axial diffusivity template
- **ENA50_DA.nii.gz**: Dispersion anisotropy index template
- **ENA50_DEM_labels.nii.gz**: Label scheme derived from the dHCP framework
- **ENA50_ENA_labels.nii.gz**: Label scheme derived from the ENA33 atlas
- **ENA50_FA.nii.gz**: Fractional anisotropy template
- **ENA50_fintra.nii.gz**: FICVF template
- **ENA50_fiso.nii.gz**: FISO template
- **ENA50_JHU_labels.nii.gz**: Label scheme derived from the JHU atlas
- **ENA50_LowerCingulum.nii.gz**: Lower Cingulum mask (fro skeletonization porpuses: TBSS/PSMD)
- **ENA50_mask.nii.gz**: Whole brain mask
- **ENA50_MCRIB_labels.nii.gz**: Label scheme derived from the M-CRIB atlas (only grey matter areas)
- **ENA50_MD.nii.gz**: Mean diffusivity template
- **ENA50_ODIp.nii.gz**: ODIp template (ODI in the primary axis)
- **ENA50_ODIs.nii.gz**: ODIs template (ODI in the secondary axis)
- **ENA50_ODItot.nii.gz**: ODItot template (overall ODI)
- **ENA50_RD.nii.gz**: Radial diffusivity template
- **ENA50_skeleton_mask.nii.gz**: WM skeleton thresholded at 0.15 and binarized
- **ENA50_skeleton_mask_psmd.nii.gz**: Custom mask for PSMD
- **ENA50_skeleton.nii.gz**: WM skeleton
- **ENA50_T1w.nii.gz**: T1w template
- **ENA50_T1w_skull.nii.gz**: T1w template with skull
- **ENA50_T2w.nii.gz**: T2w template
- **ENA50_T2w_skull.nii.gz**: T2w template with skull
- **ENA50_tensor.nii.gz**: Tensor template (compatible with DTI-TK)
- **ENA50_tissue_labels.nii.gz**: Tissue segmentation (https://github.com/MIRTK/DrawEM/blob/master/label_names/tissue_labels.csv)
- **ENA50_TPM_Background.nii.gz**: Background tissue probability map
- **ENA50_TPM_BS.nii.gz**: Braistem tissue probability map
- **ENA50_TPM_Cerebellum.nii.gz**: Cerebellum tissue probability map
- **ENA50_TPM_CSF.nii.gz**: External cerebrospinal fluid tissue probability map 
- **ENA50_TPM_dGM.nii.gz**: Subcortical grey matter tissue probability map
- **ENA50_TPM_GM.nii.gz**: Cortical grey matter tissue probability map
- **ENA50_TPM_Hippocampi_Amygdala.nii.gz**: Hippocampi and Amygdala tissue probability map
- **ENA50_TPM_mask.nii.gz**: Whole brain tissue probability map
- **ENA50_TPM_Ventricles.nii.gz**: Lateral ventricles tissue probability map
- **ENA50_TPM_WM.nii.gz**: White matter tissue probability map
- **ENA50_UNC_labels.nii.gz**: Label scheme derived from the UNC atlas
- **JHU_lut.txt**: lut table from the JHU parcellation scheme (associated to the ENA50_JHU_labels.nii.gz file)
- **MCRIB_lut.txt**: lut table from the M-CRIB parcellation scheme (associated to the ENA50_MCRIB_labels.nii.gz file)
- **UNC_lut.txt**: lut table from the UNC parcellation scheme (associated to the ENA50_UNC_labels.nii.gz file)
