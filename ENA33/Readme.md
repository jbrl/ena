# ENA33
The ENA33 includes the following files:
Asymmetric and symmetric versions of:
 - **ENA33-CSF.nii.gz**: Cerebrospinal fluid tissue probability map
 - **ENA33-dGM.nii.gz**: Subcortical grey tissue probability map
 - **ENA33-FA.nii.gz**: Fractional anisotropy template
 - **ENA33-GM.nii.gz**: Cortical grey tissue probability map
 - **ENA33-Labels.nii.gz**: Label map
 - **ENA33-Mask.nii.gz**: Whole brain mask
 - **ENA33-MD.nii.gz**: Mean diffusivity template
 - **ENA33-T1.nii.gz**: T1w template (with skull)
 - **ENA33-T2.nii.gz**: T2w template (with skull)
 - **ENA33-WM.nii.gz**: White matter tissue probability map

Lut tables:
 - **ENA33-BrainColor.seg**: Color edited lut table
 - **ENA33-BrainColor.txt**: Color edited lut table
 - **ENA33-defaultColor.seg**: Lut table
